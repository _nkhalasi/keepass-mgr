package tech.njk.keepassmgr

import arrow.effects.IO
import arrow.effects.extensions.io.monad.binding
import org.linguafranca.pwdb.kdbx.KdbxCreds
import org.linguafranca.pwdb.kdbx.simple.SimpleDatabase
import org.linguafranca.pwdb.kdbx.simple.SimpleIcon
import java.nio.file.Path
import java.nio.file.Paths

fun prompt(msg: String) = IO {
  print("$msg ")
}

fun captureUserInput() = IO {
  readLine() ?: ""
}

data class SecretEntry(
  val groupName: String,
  val title: String,
  val url: String,
  val username: String,
  val password: String
)

fun loadFile(file: Path): IO<List<SecretEntry>> = IO {
  file.toFile().bufferedReader().use {
    it.lineSequence().map {
      val data = it.split("::").map {
        it.trim()
      }
      SecretEntry(data[0], data[1], data[2], data[3], data[4])
    }.toList()
  }
}

fun String.newGroup(db: SimpleDatabase) = db.newGroup(this@newGroup).apply { icon = SimpleIcon(48) }

fun initKeeDb(dbFile: Path, creds: KdbxCreds): IO<SimpleDatabase> = IO {
  if (dbFile.toFile().exists()) {
    SimpleDatabase.load(creds, dbFile.toFile().inputStream())
  } else {
    SimpleDatabase().apply {
      name = "vaynet_retain"
      description = "Vay Network Retain"
      rootGroup.name = "Vay Network"
    }
  }
}

fun saveDBToFile(dbFile: Path, db: SimpleDatabase, creds: KdbxCreds) = IO {
  dbFile.toFile().outputStream().use {
    db.save(creds, it)
  }
}

class App {
  companion object {
    @JvmStatic
    fun main(args: Array<String>) {

      binding {
        prompt("Key File Master Password   : ").bind()
        val pwd = captureUserInput().bind()

        prompt("Primary Group              : ").bind()
        val mainGroupName = captureUserInput().bind()

        prompt("Input data file(full path) : ").bind()
        val inputFile = captureUserInput().bind()

        val secretsFile = Paths.get("/home/nkhalasi/vayana/_crds/imported.kdbx")

        val creds = KdbxCreds(pwd.toByteArray())
        val db = initKeeDb(secretsFile, creds).bind()

        val primaryGroup = db.rootGroup.groups.firstOrNull { it.name == mainGroupName } ?: db.rootGroup.addGroup(mainGroupName.newGroup(db))

        val dataToImport = loadFile(Paths.get(inputFile)).bind()
        dataToImport.forEach { entry ->
          val currentGroup = primaryGroup.groups.firstOrNull { sg ->
            sg.name == entry.groupName
          } ?: primaryGroup.addGroup(entry.groupName.newGroup(db))

          val currentEntry = currentGroup.entries.firstOrNull { et ->
            et.title == if (entry.title == "-") entry.username else entry.title
          } ?: db.newEntry()

          currentEntry.apply {
            username = if (entry.username == "-") "" else entry.username
            password = entry.password
            url = if (entry.url == "-") "" else entry.url
            title = if (entry.title == "-") entry.username else entry.title
          }
        }

        saveDBToFile(secretsFile, db, creds).bind()
        prompt("Done...\n").bind()
      }.unsafeRunSync()
    }
  }
}
