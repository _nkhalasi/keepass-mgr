import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.3.31"
  kotlin("kapt") version "1.3.31"
}

group = "tech.njk"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
  jcenter()
}

val arrowVersion = "0.9.0"
dependencies {
  runtime(kotlin("reflect"))
  implementation(kotlin("stdlib"))

  compile("org.linguafranca.pwdb", "KeePassJava2", "2.1.4")
  compile("io.arrow-kt", "arrow-core-data", arrowVersion)
  compile("io.arrow-kt", "arrow-core-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-syntax", arrowVersion)
  compile("io.arrow-kt", "arrow-typeclasses", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-data", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-data", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-io-extensions", arrowVersion)
  kapt("io.arrow-kt", "arrow-meta", arrowVersion)
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}